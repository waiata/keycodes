//
//  KeyStroke.swift
//  KeyCodes
//
//  Created by Neal Watkins on 2022/1/16.
//

import Cocoa

struct KeyStroke: CustomStringConvertible, Equatable, Codable {
    
    var key: String?
    
    var modifiers = NSEvent.ModifierFlags()
    
    var special: NSEvent.SpecialKey?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let string = try? container.decode(String.self)
        self.init(string)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(label)
    }
    
    init(_ string: String?) {
        key = string?.trimmingCharacters(in: .init(charactersIn: "⇧⌃⌥⌘")) ?? ""
        guard let string = string else { return }
        if string.contains("⇧") { modifiers.update(with: .shift) }
        if string.contains("⌃") { modifiers.update(with: .control) }
        if string.contains("⌥") { modifiers.update(with: .option) }
        if string.contains("⌘") { modifiers.update(with: .command) }
    }
    
    var label: String {
        var q = key?.capitalized ?? ""
        if modifiers.contains(.control) { q = "⌃" + q }
        if modifiers.contains(.option) { q = "⌥" + q }
        if modifiers.contains(.shift) { q = "⇧" + q }
        if modifiers.contains(.command) { q = "⌘" + q }
        return q
    }
    
    init(_ event: NSEvent) {
        switch event.specialKey {
        case NSEvent.SpecialKey.downArrow: key = "▼"
        case NSEvent.SpecialKey.backTab: key = "⇥"
        case NSEvent.SpecialKey.leftArrow: key = "◀︎"
        case NSEvent.SpecialKey.upArrow: key = "▲"
        case NSEvent.SpecialKey.rightArrow: key = "▶︎"
        case NSEvent.SpecialKey.backspace: key = "⌫"
        case NSEvent.SpecialKey.`break`: key = "break"
        case NSEvent.SpecialKey.carriageReturn: key = "⏎"
        case NSEvent.SpecialKey.delete: key = "⌫"
        case NSEvent.SpecialKey.deleteForward: key = "⌦"
        case NSEvent.SpecialKey.end: key = "⤓"
        case NSEvent.SpecialKey.enter: key = "⌤"
        case NSEvent.SpecialKey.execute: key = "Execute"
        case NSEvent.SpecialKey.f1: key = "f1"
        case NSEvent.SpecialKey.f10: key = "f10"
        case NSEvent.SpecialKey.f11: key = "f11"
        case NSEvent.SpecialKey.f12: key = "f12"
        case NSEvent.SpecialKey.f13: key = "f13"
        case NSEvent.SpecialKey.f14: key = "f14"
        case NSEvent.SpecialKey.f15: key = "f15"
        case NSEvent.SpecialKey.f16: key = "f16"
        case NSEvent.SpecialKey.f17: key = "f17"
        case NSEvent.SpecialKey.f18: key = "f18"
        case NSEvent.SpecialKey.f19: key = "f19"
        case NSEvent.SpecialKey.f2: key = "f2"
        case NSEvent.SpecialKey.f20: key = "f20"
        case NSEvent.SpecialKey.f21: key = "f21"
        case NSEvent.SpecialKey.f22: key = "f22"
        case NSEvent.SpecialKey.f23: key = "f23"
        case NSEvent.SpecialKey.f24: key = "f24"
        case NSEvent.SpecialKey.f25: key = "f25"
        case NSEvent.SpecialKey.f26: key = "f26"
        case NSEvent.SpecialKey.f27: key = "f27"
        case NSEvent.SpecialKey.f28: key = "f28"
        case NSEvent.SpecialKey.f29: key = "f29"
        case NSEvent.SpecialKey.f3: key = "f3"
        case NSEvent.SpecialKey.f30: key = "f30"
        case NSEvent.SpecialKey.f31: key = "f31"
        case NSEvent.SpecialKey.f32: key = "f32"
        case NSEvent.SpecialKey.f33: key = "f33"
        case NSEvent.SpecialKey.f34: key = "f34"
        case NSEvent.SpecialKey.f35: key = "f35"
        case NSEvent.SpecialKey.f4: key = "f4"
        case NSEvent.SpecialKey.f5: key = "f5"
        case NSEvent.SpecialKey.f6: key = "f6"
        case NSEvent.SpecialKey.f7: key = "f7"
        case NSEvent.SpecialKey.f8: key = "f8"
        case NSEvent.SpecialKey.f9: key = "f9"
        case NSEvent.SpecialKey.home: key = "⤒"
        case NSEvent.SpecialKey.next: key = "⏩"
        case NSEvent.SpecialKey.pageDown: key = "⇟"
        case NSEvent.SpecialKey.pageUp: key = "⇞"
        case NSEvent.SpecialKey.pause: key = "⏸"
        case NSEvent.SpecialKey.prev: key = "⏪"
        case NSEvent.SpecialKey.stop: key = "⏹"
        case NSEvent.SpecialKey.tab: key = "⇥"
        case NSEvent.SpecialKey.clearLine: key = "⎚"
        default:
            key = event.charactersIgnoringModifiers ?? ""
            modifiers = event.modifierFlags
        }
        switch event.keyCode {
        case 53: key = "⎋"
        default: break
        }
        
        special = event.specialKey
    }
    
    
    var description: String {
        return "KeyStroke \(label)"
    }
    
    
    
}
