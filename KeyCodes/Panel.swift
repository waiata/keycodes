//
//  Panel.swift
//  KeyCodes
//
//  Created by Neal Watkins on 2022/2/9.
//

import Cocoa

class Panel: NSPanel {

    override func cancelOperation(_ sender: Any?) {
        // disable closing of panel by hitting escape
    }
    
}
