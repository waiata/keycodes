//
//  ViewController.swift
//  KeyCodes
//
//  Created by Neal Watkins on 2022/1/16.
//

import Cocoa

class ViewController: NSViewController {
    
    @IBOutlet weak var keyStrokeLabel: NSTextField!
    @IBOutlet weak var keyCodeLabel: NSTextField!
    
    @IBAction func copyStroke(_ sender: Any) {
        
        NSPasteboard.general.clearContents()
        NSPasteboard.general.setString(keyStrokeLabel.stringValue, forType: .string)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSEvent.addLocalMonitorForEvents(matching: .flagsChanged) {
            self.flagsChanged(with: $0)
            return $0
        }
        NSEvent.addLocalMonitorForEvents(matching: .keyDown) {
            self.keyDown(with: $0)
            return $0
        }
    }
    
    override func viewDidDisappear() {
        super.viewDidDisappear()
        NSApplication.shared.terminate(self)
    }
    
    override func keyDown(with event: NSEvent) {
        keyCodeLabel.stringValue = "\(event.keyCode)"
        let stroke = KeyStroke(event)
        keyStrokeLabel.stringValue = stroke.label
    }
    
}

